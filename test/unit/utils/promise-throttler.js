describe('unit/utils/promise-throttler.js', function() {
	var promiseThrottler = require('../../../src/utils/promise-throttler')
	describe('When calling the promise-buffer with less than max', function() {
		it('should not throw', function() {
			expect(function() {
				var promises = [
					Promise.resolve(),
					Promise.resolve(),
					Promise.resolve(),
					Promise.resolve(),
				]
				promiseThrottler(promises, promises.length +2)
			}).not.to.throw()
		})
	})
	describe('When calling the promise-buffer', function() {
		var promiseGenerators
		var result
		var resolvedPromises

		describe('and all promises are resolved', function() {
			var concurrent
			var maxConcurrent
			beforeEach(function() {
				concurrent = 0
				maxConcurrent = 0
				resolvedPromises = 0
				promiseGenerators = [
					10, 20, 30, 20, 10,
				].map(function(timeout) {
					return function() {
						concurrent++
						return new Promise(function(resolve, reject) {
							setTimeout(resolve, timeout)
						}).then(function() {
							maxConcurrent = Math.max(maxConcurrent, concurrent)
							concurrent--
							resolvedPromises++
						})
					}
				})
				result = promiseThrottler(promiseGenerators, 3)
			})

			it('should resolve when all promises do', function() {
				return result.then(function() {
					resolvedPromises.should.equal(promiseGenerators.length)
				})
			})
			it('should never run more than max at once', function() {
				return result.then(function() {
					maxConcurrent.should.equal(3)
				})
			})
		})

		describe('and a promise fails', function() {
			var startedPromises
			beforeEach(function() {
				resolvedPromises = 0
				startedPromises = 0
				promiseGenerators = [
					100, 2, 300, 200, 100,
				].map(function(timeout, idx) {
					return function() {
						startedPromises++
						return new Promise(function(resolve, reject) {
							setTimeout(resolve, timeout)
						}).then(function() {
							if(idx == 1) throw new Error('failing promise')
							resolvedPromises++
						})
					}
				})
				result = promiseThrottler(promiseGenerators, 3)
			})

			it('should fail if a promise do', function() {
				return result.should.eventually.be.rejected
			})
			it('should not start new if a promise got rejected', function() {
				return result.catch(function() {})
					.then(function() {
						startedPromises.should.equal(3)
					})
			})
		})
	})
})
