import Foundation

enum ExitCode : Int32 {
	case ArgumentMissing = 1
	case FileNotFound = 2
}

if Process.arguments.count < 2 {
	print("Usage: bplist [plist-file]")
	exit(ExitCode.ArgumentMissing.rawValue)
}

let path = Process.arguments[1]

if let dict = NSDictionary(contentsOfFile:path) {
	let jsonData = try! NSJSONSerialization.dataWithJSONObject(dict, options:.PrettyPrinted)
	let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
	print(jsonString)
} else {
	print("File at path \(path) could not be read as PList")
	exit(ExitCode.FileNotFound.rawValue)
}
