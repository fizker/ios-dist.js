var settings = require('./src/settings')

var PORT = process.env.PORT || 8086
var BUILD_DIR = process.env.IOS_BUILD_DIR || '/Users/Shared/Sites/poteo/'
var IOS_DIR = process.env.IOS_CODE_DIR || '/Users/benjamin/Development/poteo/iphone'
var distServer = null

if(process.env.DIST_SERVER=='s3') {
	distServer = {
		type: 's3',
		requestSettings: {
			headers: {
				'x-amz-acl': 'public-read',
			},
			aws: {
				bucket: process.env.AWS_BUCKET,
				key: process.env.AWS_ACCESS_KEY,
				secret: process.env.AWS_SECRET_KEY,
			},
			followAllRedirects: true,
		},
		url: 'https://' + process.env.AWS_BUCKET + '.s3.amazonaws.com',
	}
}

settings._set({
	port: PORT,
	iosCodeDir: IOS_DIR,
	downloadableAppsDir: BUILD_DIR,
	distServer: distServer,
})


var server = require('./src/server')

if(require.main !== module) {
	// Included from tests or other node.js modules
	module.exports = {
		open: server,
	}
	return
}

require('./src/git').checkout('HEAD').then(function() {
	return require('./src/handle-git').fetch()
}).catch(function(e) {console.log(e)})

server()
	.then(function(server) {
		console.log('Running at port %s', PORT)
	})
	.done()
