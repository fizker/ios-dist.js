var React = require('react')
var merge = require('fmerge')
var router = require('react-router')

var Apps = require('./apps.component')
var BuildStatus = require('./widgets/build-status.component')

module.exports = React.createClass({ displayName: 'Main',
	render: function() {
		return (
			<div className="main-page">
				<nav>
					<h2>Apps</h2>
					<a href="http://returntool-ios.s3.amazonaws.com/TargetSelector.app.zip">TargetSelector helper</a>
					<BuildStatus />
					<Apps />
				</nav>
				<div className="main-page__body">
					<router.RouteHandler params={this.props.params}/>
				</div>
			</div>
		)
	},
})
