var React = require('react')

var buildStore = require('../../src/stores/build')
var storeListener = require('../../src/mixins/store-listener')

module.exports = React.createClass({
	mixins: [
		storeListener(buildStore, function() {
			return {
				builds: buildStore.getAll(),
				currentBuild: buildStore.getCurrent(),
			}
		})
	],
	render: function() {
		var currentBuild = this.state.currentBuild
		return (
			<div>
				Build queue: {this.state.builds.length == 0
					? 'Empty'
					: `${this.state.builds.length} elements`}
				{currentBuild && <div
					className="progress-bar"
					style={{width:(currentBuild.progress*100)+'%'}}
					/>}
			</div>
		)
	},
})
