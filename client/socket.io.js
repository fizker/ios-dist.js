var constants = require('../src/constants')
var dispatcher = require('../src/dispatcher')

var socket = io.connect('/')
socket
	.on('connect', function() {
		dispatcher.handleMaintenanceAction({
			type: dispatcher.constants.CAPABILITY_ADDED,
			capability: constants.capabilities.SOCKET_IO,
		})
	})
	.on('connect_error', function(err) {
		dispatcher.handleMaintenanceAction({
			type: dispatcher.constants.CAPABILITY_REMOVED,
			capability: constants.capabilities.SOCKET_IO,
		})
	})
	.on('build-queue', function(builds) {
		dispatcher.handleServerAction({
			type: dispatcher.constants.BUILD_QUEUE_UPDATED,
			builds,
		})
	})
	.on('build-started', function(build) {
		dispatcher.handleServerAction({
			type: dispatcher.constants.BUILD_STARTED,
			build,
		})
	})
	.on('build-updated', function(build) {
		dispatcher.handleServerAction({
			type: dispatcher.constants.BUILD_PROGRESSED,
			build,
		})
	})
	.on('build-finished', function(build) {
		dispatcher.handleServerAction({
			type: dispatcher.constants.BUILD_ENDED,
			build,
		})
	})
