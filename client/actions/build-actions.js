var fajax = require('fajax')
var dispatcher = require('../../src/dispatcher')

module.exports = {
	requestBuild
}

function requestBuild(app, branch) {
	fajax.post('/builds', { json: { app: app.name, branch } })
		.then(xhr => xhr.body)
		.then(builds => dispatcher.handleViewAction({
			type: dispatcher.constants.BUILD_QUEUE_UPDATED,
			builds,
		}))
		.done()
}
