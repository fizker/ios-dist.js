var React = require('react')
var router = require('react-router')

var stores = require('../src/stores')
var appStore = stores.app
var storeListener = require('../src/mixins/store-listener')

var buildConstants = require('../src/build-constants')

module.exports = React.createClass({ displayName: 'Apps',
	mixins: [storeListener(appStore)],
	getInitialState: function() {
		return {
			filter: '',
			showDownloadablesOnly: !buildConstants.allowBuilds
		}
	},
	render: function() {
		var unfilteredApps = this.state.showDownloadablesOnly
			? appStore.getAllWithDownloads()
			: appStore.getAll()
		var apps = unfilteredApps
			.filter(app => app.name.toLowerCase().includes(this.state.filter))
		return (
			<div className="app-list">
				<input
					type="search"
					placeholder="Filter apps"
					onChange={this.onFilter}
				/>
				{ buildConstants.allowBuilds &&
				<br />
				}
				{ buildConstants.allowBuilds &&
				<label>
					<input
						type="checkbox"
						checked={this.state.showDownloadablesOnly}
						onChange={this.onToggleDownloadablesOnly}
					/>
					Show only apps with downloads
				</label>
				}
				<div className="app-list__apps">
					{apps.map(app =>
						<router.Link
							to="app"
							key={app.name}
							params={{app:app.name}}
							className="app-list__apps__app"
						>
							{false && <img src={`/apps/${app.name}/icon`}/>}
							{app.name}
						</router.Link>
					)}
				</div>
			</div>
		)
	},

	onToggleDownloadablesOnly: function(event) {
		this.setState({
			showDownloadablesOnly: event.target.checked,
		})
	},

	onFilter: function(event) {
		this.setState({
			filter: event.target.value.toLowerCase()
		})
	},
})
