var React = require('react')

var buildActions = require('./actions').build

var stores = require('../src/stores')
var storeListener = require('../src/mixins/store-listener')

var buildConstants = require('../src/build-constants')

function getDataFromStores() {
	return {
		apps: stores.app.getAll(),
		branches: stores.branch.getAll()
			.filter((branch, idx, arr)=>idx==arr.findIndex(b=>b.head==branch.head)),
		builds: stores.build.getAll(),
	}
}

module.exports = React.createClass({ displayName: 'App',
	mixins: [storeListener(stores, getDataFromStores)],
	propTypes: {
		params: React.PropTypes.shape({
			app: React.PropTypes.string.isRequired,
		}).isRequired,
	},
	getInitialState: function() {
		return {
			download: null,
			build: null,
		}
	},
	componentWillReceiveProps(nextProps) {
		if(nextProps.params.app != this.props.params.app) {
			this.setState(this.getInitialState())
		}
	},
	render: function() {
		var branches = this.state.branches || []
		var currentApp = this.getApp()
		if(!currentApp) {
			return <div className="app">No data</div>
		}

		var allDownloads = currentApp.downloads.sort((a, b) => new Date(b.date) - new Date(a.date))
		var availableBuilds = branches
			.filter(branch => !allDownloads.find(d => d.revision == branch.head))
		var currentBuild = this.state.build || availableBuilds[0]
		var currentDownload = this.state.download
			? allDownloads.find(d => d.revision == this.state.download.revision)
			: allDownloads[0]

		var allowBuilds = buildConstants.allowBuilds

		return (
			<div className="app">
				<h2>
					<img
						className="app__icon"
						src={ currentApp.downloads.length > 0 && currentApp.downloads[0].links
							? currentApp.downloads[0].links.icon
							: `/apps/${currentApp.name}/icon`
						}
					/>
					{currentApp.name}
				</h2>
				{ currentApp.downloads.length > 0
				? <div>
					{currentApp.downloads[0].links ? 'Ready for download' : 'Built but undownloadable'}:
					<select value={currentDownload.revision} onChange={this.onChangeDownload}>
						<optgroup label="Dev branches"/>
						{allDownloads
							.filter(d => !d.version)
							.map(download =>
								<option key={download.revision} value={download.revision}>
									{`${download.branch || download.revision} (${download.date})`}
								</option>
							)
						}
						<optgroup label="App store"/>
						{allDownloads
							.filter(d => d.version)
							.map(download =>
								<option key={download.revision} value={download.revision}>
									{download.version}
								</option>
							)
						}
					</select>
					{currentApp.downloads[0].links && <a href={`itms-services://?action=download-manifest&url=${currentDownload.links.plist}`}>
						Download
					</a>}
				</div>
				: <div>No builds ready for download</div>
				}
				{ allowBuilds && (currentBuild
				? <div>Available builds:
					<select value={currentBuild.name} onChange={this.onChangeBuild}>
						{availableBuilds.map(build => <option key={build.head}>{build.name}</option>)}
					</select>
					<button onClick={this.onRequestBuild.bind(null, currentBuild)}>Request build</button>
				</div>
				: <div>This app is fully built</div>
				)}
			</div>
		)
	},

	getApp: function() {
		if(!this.state.apps) {
			return null
		}

		var appName = this.props.params.app
		return this.state.apps.find(app => app.name == appName)
	},

	onRequestBuild: function(branch) {
		var app = this.getApp()
		buildActions.requestBuild(app, branch)
	},

	onChangeDownload: function(event) {
		this.setState({
			download: this.getApp().downloads.find(download =>
				download.revision == event.target.value
			),
		})
	},
	onChangeBuild: function(event) {
		var build = this.state.branches.find(build => build.name == event.target.value)
		this.setState({
			build
		})
	},
})
