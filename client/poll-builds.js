var fajax = require('fajax')
var constants = require('../src/constants')
var dispatcher = require('../src/dispatcher')
var stores = require('../src/stores')

// This just needs to be loaded, it will automatically attach itself to the system.
module.exports = {}

var currentBuildTimer
var hasSocketIO = stores.capabilities.hasCapability(constants.capabilities.SOCKET_IO)

checkForStart()


stores.capabilities.addChangeListener(function() {
	hasSocketIO = stores.capabilities.hasCapability(constants.capabilities.SOCKET_IO)
	checkForStart()
})

function checkForStart() {
	if(hasSocketIO) {
		stopBuildTimer()
	} else {
		if(stores.build.getCurrent()) {
			startBuildTimer()
		}
	}
}

function stopBuildTimer() {
	if(currentBuildTimer) {
		clearInterval(currentBuildTimer)
		currentBuildTimer = null
	}
}

function startBuildTimer() {
	currentBuildTimer = setInterval(function() {
		fajax.get('/builds').then(function(xhr) {
			var currentBuild = stores.build.getCurrent()
			var builds = xhr.body
			dispatcher.handleServerAction({
				type: dispatcher.constants.BUILD_QUEUE_UPDATED,
				builds: builds,
			})

			var currentUpdated = builds.find(build => build.progress)
			if(currentUpdated) {
				if(currentBuild) {
					if(currentBuild.app == currentUpdated.app && currentBuild.revision == currentUpdated.revision) {
						dispatcher.handleServerAction({
							type: dispatcher.constants.BUILD_PROGRESSED,
							build: currentUpdated,
						})
					} else {
						dispatcher.handleServerAction({
							type: dispatcher.constants.BUILD_ENDED,
							build: currentBuild,
						})
						dispatcher.handleServerAction({
							type: dispatcher.constants.BUILD_STARTED,
							build: currentUpdated,
						})
					}
				} else {
					dispatcher.handleServerAction({
						type: dispatcher.constants.BUILD_STARTED,
						build: currentUpdated,
					})
				}
			} else if(currentBuild) {
				dispatcher.handleServerAction({
					type: dispatcher.constants.BUILD_ENDED,
					build: currentBuild,
				})
			}
		})
	}, 500)
}

dispatcher.register(function(payload) {
	var action = payload.action
	switch(action.type) {
		case dispatcher.constants.BUILD_QUEUE_UPDATED:
			var current = stores.build.getCurrent()
			var progress = action.builds.find(build => build.progress != null)
			if(progress) {
				if(current && current.app == progress.app && current.revision == progress.revision) {
					return
				}
				dispatcher.handleServerAction({
					type: dispatcher.constants.BUILD_STARTED,
					build: progress,
				})
			}
			break
		case dispatcher.constants.BUILD_STARTED:
			checkForStart()
			break
		case dispatcher.constants.BUILD_ENDED:
			stopBuildTimer()
			break
	}
})
