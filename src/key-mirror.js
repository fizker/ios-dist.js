module.exports = keyMirror

function keyMirror(keys) {
	if(typeof(keys) == 'string') {
		return keyMirror([ keys ])
	}
	if(!Array.isArray(keys)) {
		return keyMirror(Object.keys(keys))
	}

	return keys.reduce(function(obj, key) {
		obj[key] = key
		return obj
	}, {})
}
