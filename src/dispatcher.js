var constants = require('./constants')

var callbacks = []

module.exports = {
	register: function(callback) {
		callbacks.push(callback)
		return callbacks.length - 1
	},
	unregister: function(id) {
		delete callbacks[id]
	},
	dispatch: function(payload) {
		return callbacks.map(function(callback) {
			try {
				callback(payload)
			} catch(e) {
				return e
			}
		}).filter(function(e) {
			return !!e
		})
	},

	constants: constants.actionTypes,

	handleRouteAction: function(action) {
		return this.dispatch({
			source: 'ROUTE_ACTION',
			action: action,
		})
	},
	handleViewAction: function(action) {
		return this.dispatch({
			source: 'VIEW_ACTION',
			action: action,
		})
	},
	handleServerAction: function(action) {
		return this.dispatch({
			source: 'SERVER_ACTION',
			action: action,
		})
	},
	handleMaintenanceAction: function(action) {
		return this.dispatch({
			source: 'MAINTENANCE_ACTION',
			action: action,
		})
	},
}
