module.exports = branch

var Q = require('q')
var exec = Q.denodeify(require('child_process').exec)

var settings = require('../settings')

function branch() {
	return exec('git branch --no-color', { cwd: settings.iosCodeDir })
		.catch(function(err) {
			throw new Error('Failed when fetching branches: ' + err.message)
		})
		.spread(function(stdout, stderr) { return stdout.trim().split('\n')
			.map(function(branch) {
				return branch.replace(/^\*/, '').trim()
			})
			.filter(function(branch) { return branch })
			.sort(function(a,b) {
				if(a == 'master') return -1
				if(b == 'master') return 1
				return a < b ? -1 : 1
			})
		})
}
