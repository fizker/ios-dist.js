module.exports = clone

var Q = require('q')
var exec = Q.denodeify(require('child_process').exec)

var settings = require('../settings')

function clone(path) {
	return exec('git clone --recursive "'+settings.iosCodeDir+'" "'+path+'"')
		.catch(function(err) {
			throw new Error('Failed when cloning repo: ' + err.message)
		})
		.thenResolve()
}
