module.exports = {
	branch: require('./branch'),
	checkout: require('./checkout'),
	clone: require('./clone'),
	fetch: require('./fetch'),
	tag: require('./tag'),
}
