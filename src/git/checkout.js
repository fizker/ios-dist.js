var Q = require('q')
var exec = Q.denodeify(require('child_process').exec)

var settings = require('../settings')

module.exports = function(committish, cwd) {
	if(!cwd) {
		cwd = settings.iosCodeDir
	}
	return exec('git checkout --detach -f '+committish, { cwd: cwd })
		.catch(function(err) {
			throw new Error('Failed when checking out commit: ' + err.message)
		})
		.thenResolve()
}
