module.exports = branch

var Q = require('q')
var exec = Q.denodeify(require('child_process').exec)

var settings = require('../settings')

function branch(options) {
	var opts = ''
	if(!options) options = {}
	/* Supporting .all would entail handling duplicate branches (existing both
	 * locally and remotely)
	 *

	if(options.all) {
		opts += '--all '
	}
	*/
	if(options.remotes) {
		opts += '--remotes '
	}

	var branchRegex = /^((remotes\/)?(.+?)\/)?(.+?)\s+([0-9a-f]+)/
	var gitOpts = { cwd: settings.iosCodeDir }
	return exec('git branch --no-color --verbose ' + opts, gitOpts)
		.catch(function(err) {
			throw new Error('Failed when fetching branches: ' + err.message)
		})
		.spread(function(stdout, stderr) {
			var branches = stdout.trim().split('\n')
				.map(function(branch){ return branch.replace(/^\*/, '').trim() })
				.filter(function(branch) { return branch && branch[0] != '(' })
				.map(function(branch) {
					var details = branch.match(branchRegex)
					if(!details) return
					return {
						remote: details[3],
						name: details[4],
						head: details[5],
					}
				})
				.filter(function(branch) {
					return branch && branch.name != 'HEAD'
				})
				.sort(function(a,b) {
					if(a.name == 'master') return -1
					if(b.name == 'master') return 1
					return a.name < b.name ? -1 : 1
				})
			return branches
		})
}
