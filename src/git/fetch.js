var Q = require('q')
var exec = Q.denodeify(require('child_process').exec)

var settings = require('../settings')

module.exports = function(cwd) {
	if(!cwd) {
		cwd = settings.iosCodeDir
	}
	return exec('git fetch --prune', { cwd: cwd })
		.catch(function(err) {
			throw new Error('Failed when fetching data: ' + err.message)
		})
		.spread(function(stdout, stderr) {
			var lines = stderr.split('\n')
			return lines.slice(1).map(function(line) {
				var newTagMatch = line.match(/^ \* \[new tag\]\s+(.*)\s+-> (.*)$/)
				if(newTagMatch) {
					return {
						type: 'new-tag',
						tag: newTagMatch[1].trim(),
					}
				}
				var tagUpdate = line.match(/^ - \[tag update\]\s+(.*)\s+-> (.*)$/)
				if(tagUpdate) {
					return {
						type: 'tag-update',
						tag: tagUpdate[1].trim(),
					}
				}
				var newBranchMatch = line.match(/^ \* \[new branch\]\s+(.*)\s+-> (.*)\/(.*)$/)
				if(newBranchMatch) {
					return {
						type: 'new-branch',
						branch: newBranchMatch[1].trim(),
						remote: newBranchMatch[2].trim(),
					}
				}
				var updatedBranchMap = line.match(/^   ([a-f0-9]+)..([a-f0-9]+)\s+(.*)\s+-> (.*)\/(.*)$/)
				if(updatedBranchMap) {
					return {
						type: 'updated-branch',
						branch: updatedBranchMap[3].trim(),
						from: updatedBranchMap[1].trim(),
						to: updatedBranchMap[2].trim(),
						remote: updatedBranchMap[4].trim(),
					}
				}
				var deletedBranch = line.match(/^.-.\[deleted\]\s+\(none\)\s+-> (.*)\/(.*)$/)
				if(deletedBranch) {
					return {
						type: 'deleted-branch',
						branch: deletedBranch[2].trim(),
						remote: deletedBranch[1].trim(),
					}
				}

				return {
					type: 'unknown',
					line: line,
				}
			})
		})
}
