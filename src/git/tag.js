var Q = require('q')
var exec = Q.denodeify(require('child_process').exec)

var settings = require('../settings')
var promiseThrottler = require('../utils/promise-throttler')

module.exports = function() {
	var gitOpts = { cwd: settings.iosCodeDir }
	return exec('git tag', gitOpts)
		.catch(function(err) {
			throw new Error('Failed when fetching tags: ' + err.message)
		})
		.spread(function(stdout, stderr) { return stdout.trim()
			.split('\n')
			.map(function(tag) {
				var v = tag.split(/-(?=[0-9])/)
				if(v.length == 1) {
					v = [ null, v[0] ]
				}
				return {
					app: v[0],
					version: v[1],
					tag: tag,
				}
			})
			.sort(function(a,b) {
				if(!a.app) return -1
				if(!b.app) return 1
				if(a.app < b.app) return -1
				if(b.app < a.app) return 1
				if(a.version < b.version) return 1
				return -1
			})
		})
		.then(function(tags) {
			var revisions = tags.map(function(tag) {
				return function() {
					return exec('git log --format=%H -1 ' + tag.tag, gitOpts)
						.catch(function(err) {
							throw new Error('Failed when getting revisions' + err.message)
						})
						.spread(function(stdout, stdin) {
							tag.revision = stdout.trim()
							return tag
						})
				}
			})
			return promiseThrottler(revisions)
		})
}

module.exports.find = function(app) {
	var tags = module.exports()
	if(app) {
		tags = tags.invoke('filter', function(version) {
			return version.app == app
		})
	}
	return tags
}

module.exports.getVersions = function() {
	return module.exports()
		.then(function(versions) { return versions
			.filter(function(version, idx, arr) {
				return idx == arr.findIndex(function(o) {
					return o.app == version.app
				})
			})
		})
}
