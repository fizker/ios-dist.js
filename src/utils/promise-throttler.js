var MAX_OPEN = 5

var Q = require('q')

module.exports = function(fns, max) {
	var maxOpen = max || MAX_OPEN
	var defers = fns.map(function(fn) {
		var defer = Q.defer()
		defer.promise = defer.promise.then(fn)
		return defer
	})

	var p = Promise.all(defers.map(function(defer) {
		return defer.promise
	}))

	for(var i = 0; i < maxOpen; i++) {
		startNext()
	}

	return p

	function startNext() {
		var defer = defers.shift()
		if(!defer) return

		defer.resolve()
		defer.promise.then(startNext, function() {
			defers = []
		})
	}
}
