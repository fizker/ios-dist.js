'use strict'

class HTTPError extends Error {
	constructor(statusCode, message) {
		super(message)
		this.statusCode = this.status = statusCode
	}
}
class HTTPNotFoundError extends HTTPError {
	constructor(message) {
		super(404, message)
	}
}

class AppNotFoundError extends HTTPNotFoundError {
	constructor(message) {
		super('App not found: ' + message)
	}
}
class AppBuildNotFoundError extends HTTPNotFoundError {
	constructor(message) {
		super('App has not been built on that version: ' + message)
	}
}
class BranchNotFoundError extends HTTPNotFoundError {
	constructor(message) {
		super('Branch does not exist: ' + message)
	}
}

class BuildsNotAllowedError extends Error {}

module.exports = {
	HTTPError,
	HTTPNotFoundError,
	apps: {
		NotFound: AppNotFoundError,
		BuildNotFound: AppBuildNotFoundError,
	},
	BranchNotFound: BranchNotFoundError,
	BuildsNotAllowedError,
}
