'use strict'

module.exports = {
	getAll: getAll,
	get: get,
	reloadS3: reloadS3,
}

var path = require('path')
var fs = require('fs')
var readdir = path => new Promise((resolve, reject) => {
	fs.readdir(path, (err, files) => err ? reject(err) : resolve(files))
})

const constants = require('../build-constants')

var settings = require('../settings')
var git = require('../git')
var errors = require('../errors')
const BuildsNotAllowedError = errors.BuildsNotAllowedError

var s3 = require('./read-s3')
var local = require('./read-local')

var s3Content = null

function reloadS3() {
	if(!settings.distServer || settings.distServer.type != 's3') {
		return Promise.reject(new Error('S3 not enabled for this server'))
	}
	s3Content = null
	return getAll()
}

function get(name) {
	return getAll().then(apps => {
		var app = apps.find(app => app.name == name)
		if(app) return app
		throw new errors.apps.NotFound(name)
	})
}

function getAll() {
	return Promise.all([
		getAvailableDownloads(),
		getAvailableBuilds().catch(e => {
			if(!(e instanceof BuildsNotAllowedError)) {
				throw e
			}
		}),
	])
		.then(arr/* ([allDownloads, {apps, branches, tags} = {}]) */ => {
			let allDownloads = arr[0]
			let allBuilds = arr[1] || {}
			let tags = allBuilds.tags
			let apps = allBuilds.apps || []
			let branches = allBuilds.branches

			return allDownloads
				.reduce((all, current) => {
					let app = all.find(app => app.name == current.app)
					if(!app) {
						app = { name: current.app, downloads: [] }
						all.push(app)
					}

					if(settings.distServer && current.path) {
						let baseLink = settings.distServer.url + '/' + current.path
						current.links = {
							base: baseLink,
							bigIcon: baseLink + '/itunes.png',
							plist: baseLink + '/install.plist',
							icon: baseLink + '/itunes.png',
							ipa: baseLink + '/app.ipa',
						}
					}
					app.downloads.push(current)

					if(tags) {
						app.versions = tags
							.filter(v => v.app == app.name)
							.map(v => v.version)
					}

					all[app.name] = app
					return all
				}, apps.map(app => {
					app.downloads = []
					return app
				}))
		})
}

function getAvailableBuilds() {
	if(!constants.allowBuilds) {
		return Promise.reject(new BuildsNotAllowedError('builds not allowed according to local config'))
	}

	return Promise.all([
		readdir(path.join(settings.iosCodeDir, 'products'))
			.then(dirs => dirs
				.filter(dir => dir[0] != '.')
				// For some reason, the app bugs out if there are dirs with uppercase names.
				// In that case, we then just don't show them.
				.filter(dir => dir.toLowerCase() == dir)
			),
		git.branch({ remotes: true }),
		git.tag(),
	]).then(arr/* ([apps, branches, tags]) => ({ apps, branches, tags })) */ => ({
		apps: arr[0].map(name => ({ name })),
		branches: arr[1],
		tags: arr[2],
	}))
}

function getAvailableDownloads() {
	if(!settings.distServer) return local()

	if(!s3Content) s3Content = s3()
	return s3Content
}
