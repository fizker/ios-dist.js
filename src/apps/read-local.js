module.exports = getBuildDetails

var Q = require('q')
var path = require('path')
var fs = require('fs')

var stat = Q.nbind(fs.stat, fs)
var readdir = Q.nbind(fs.readdir, fs)

var settings = require('../settings')

function getBuildDetails() {
	var buildDir = settings.downloadableAppsDir
	var appBuildFolders = getAppList(buildDir)

	return appBuildFolders
		.then(function(builds) {
			return builds.sort(function(a,b) { return a.app < b.app ? -1 : 1 })
		})
}

function getAppList(buildDir) {
	return listFolders(buildDir)
		.then(function(apps) {
			var buildsInAllApps = apps.map(function(app) {
				var buildsForApp = listFolders(app.path)
				return buildsForApp.then(function(builds) {
					return builds.map(function(build) {
						return {
							revision: build.file,
							app: app.file,
						}
					})
				})
			})
			return Promise.all(buildsInAllApps)
				.then(function(apps) {
					return [].concat.apply([], apps)
				})
		})
}

function listFolders(path) {
	return readdir(path)
		.then(function(files) {
			return ensureFolders(path, files)
		})
}

function ensureFolders(base, paths) {
	var potentials = paths.filter(function(path) {
		return path[0] != '.'
	}).map(function(p) {
		return {
			path: path.join(base, p),
			file: p,
		}
	})
	return Promise.all(potentials.map(function(path) {
		return stat(path.path)
	})).then(function(stats) {
		return potentials.filter(function(path, idx) {
			return stats[idx].isDirectory()
		})
	})
}
