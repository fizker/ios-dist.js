'use strict'

var request = require('request')
var xml2js = require('xml2js')

var settings = require('../settings')

function requestFromS3(lastPath) {
	const url = settings.distServer.url + (lastPath ? '/' + lastPath : '')
	return new Promise((resolve, reject) => {
		request.get(url, settings.distServer.requestSettings, (err, response) => {
			if(err) return reject(err)
			if(response.statusCode >= 300) return reject(new Error('Error when getting: ' + response.statusCode + '\n' + response.body))

			resolve(response.body)
		})
	})
	.catch(e => {
		console.error(`Could not fetch ${url}: ${e.message}`)
		throw e
	})
}

module.exports = function() {
	if(!settings.distServer || settings.distServer.type != 's3') {
		return Promise.reject(new Error('S3 is not set up'))
	}
	return requestFromS3()
		.then(function(xml) {
			return new Promise(function(resolve, reject) {
				xml2js.parseString(xml, function(err, content) {
					if(err) return reject(err)
					resolve(content)
				})
			})
		})
		.then(content => content.ListBucketResult.Contents
			// Name of item
			.map(item => item.Key[0])
			.filter(path => path.endsWith('metadata.json'))
			.map(path => requestFromS3(path)
				.then(JSON.parse)
				.then(data => {
					data.path = path.slice(0, -'/metadata.json'.length)
					return data
				})
			)
		)
		.then(a => Promise.all(a))
		.then(data => data.map(d => {
			d.app = d.name
			return d
		}))
}
