var fmerge = require('fmerge')

var config = {
	allowBuilds: true
}

try {
	config = fmerge(config, require('../config.json'))
} catch(e) {}

module.exports = config
