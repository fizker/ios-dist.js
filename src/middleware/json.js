var bodyParser = require('body-parser')

module.exports = function(parseBody) {
	return function(req, res, next) {
		if(req.accepts('html')) return next('route')
		if(!parseBody) {
			return next()
		}
		bodyParser.json()(req, res, next)
	}
}