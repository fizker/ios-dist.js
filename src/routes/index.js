var express = require('express')

var router = express.Router()
module.exports = router

router.use('/apps', require('./apps'))
router.use('/branches', require('./branches'))
router.use('/builds', require('./builds'))

router.post('/update-git', function(req, res) {
	res.send(require('../handle-git').fetch().then(function() {
		res.status(204).end()
	}))
})
