var express = require('express')
var bodyParser = require('body-parser')

var builds = require('../builds')
var apps = require('../apps')

var router = express.Router()

module.exports = router

router.get('/', function(req, res) {
	res.send(builds.getQueue())
})
router.post('/', bodyParser.json(), function(req, res) {
	var app = req.body.app
	var build = req.body.branch

	res.send(apps.get(req.body.app)
	.then(function(app, branch) {
		return { app: app.name, revision: build.head, branch: build.name }
	})
	.then(builds.addToQueue))
})
