var express = require('express')
var git = require('../git')

var router = express.Router()
module.exports = router

router.get('/', function(req, res) {
	res.send(git.branch({ remotes: true }))
})
