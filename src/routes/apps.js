var Q = require('q')
var path = require('path')
var plist = require('plist')
var express = require('express')

var middleware = require('../middleware')
var git = require('../git')
var apps = require('../apps')
var errors = require('../errors')
var settings = require('../settings')
var PORT = settings.port

var router = express.Router()
module.exports = router

router.post('/reload', function(req, res) {
	res.send(apps.reloadS3()
		.then(function() {
			return 'Reloaded from S3\n'
		}))
})

var appRouter = express.Router()
router.use('/:app', appRouter)

appRouter.get('/icon', function(req, res) {
	var app = req.$.app
	res.sendFile(path.join(settings.iosCodeDir, 'products', app.name, 'Icon120.png'))
})

router.param('app', function(req, res, next, app) {
	if(!req.$) req.$ = {}
	apps.get(app)
		.then(function(app) { req.$.app = app })
		.then(apps=>next(null, apps), next)
})

router.get('/:app?', middleware.json(), function(req, res) {
	res.send(req.params.app
		? apps.get(req.params.app)
		: apps.getAll()
	)
})
