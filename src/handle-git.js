module.exports = {
	fetch: fetch,
}

var git = require('./git')

function fetch() {
	return git.fetch()
		.then(function(updates) {
			return updates
				.filter(function(update) {
					return update.type == 'new-tag'
						|| update.type == 'tag-update'
				})
				.map(function(update) {
					var parsedTag = update.tag.split('-')
					update.app = parsedTag[0]
					update.version = parsedTag[1]
					return update
				})
		})
		.then(function(appStoreUpdates) {
			// loop over these and add to build-queue
			console.log(appStoreUpdates)
		})
}
