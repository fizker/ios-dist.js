module.exports = open

var http = require('http')
var express = require('express')
var path = require('path')
var Q = require('q')
var expressPromise = require('express-promise')
var logger = require('morgan')
var compression = require('compression')

var routes = require('./routes')
var sockets = require('./sockets')

var settings = require('./settings')

function open() {
	var app = express()
	var server = http.createServer(app)
	sockets(server)

	app.set('port', settings.port)
	app.set('port.external', settings.externalPort || settings.port)

	app.use(compression())
	app.use(logger('dev'))

	app.use(expressPromise())

	app.use(routes)

	app.use(express.static(path.join(__dirname, '../static')))
	app.use(function(req, res, next) {
		if(!req.accepts('html')) return next()
		res.sendFile(path.join(__dirname, '../static/index.html'))
	})

	return Q.ninvoke(server, 'listen', settings.port)
		.thenResolve({
			close: Q.nbind(server.close, server),
		})
}
