module.exports = { _set: _set }

function _set(settings) {
	Object.keys(settings).forEach(function(key) {
		module.exports[key] = settings[key]
	})
}
