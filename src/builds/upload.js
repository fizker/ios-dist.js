var path = require('path')
var Q = require('q')
var fs = require('fs')
var stat = Q.denodeify(fs.stat)
var request = require('request')

var settings = require('../settings')
var uploadRequest = null
if(settings.distServer) {
	uploadRequest = request.defaults(settings.distServer.requestSettings)
}

module.exports = uploadFile

function uploadFile(file) {
	return stat(file.localPath)
		.then(function(stat) {
			return new Promise(function(resolve, reject) {
				fs.createReadStream(file.localPath).pipe(
					uploadRequest.put(file.remotePath, {
						headers: {
							'content-length': stat.size,
							'content-type': getMimeType(file.name),
						}
					}, function(err, response) {
						if(err) return reject(err)
						if(response.statusCode >= 300) {
							return reject(new Error('Bad status for ' + file.name +
								': ' + response.statusCode + '\n' + response.body));
						}
						resolve()
					})
				)
			})
		})
}

function getMimeType(file) {
	switch(path.extname(file)) {
		case '.png':
			return 'image/png'
		case '.ipa':
			return 'application/octet-stream'
		case '.plist':
			return 'text/xml'
		case '.json':
			return 'application/json'
		default: throw new Error('Unknown mime for file: '+ file)
	}
}
