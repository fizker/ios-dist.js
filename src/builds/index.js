module.exports = {
	getQueue: getQueue,
	addToQueue: addToQueue,
}

var Q = require('q')
var xcode = require('xcode-build-tool')
var fs = require('fs-extra')
var copy = Q.denodeify(fs.copy)
var mkdirp = Q.denodeify(fs.mkdirp)
var rimraf = Q.denodeify(fs.remove)
var readFile = Q.denodeify(fs.readFile)
var readdir = Q.denodeify(fs.readdir)
var path = require('path')
var exec = Q.denodeify(require('child_process').exec)
var merge = require('fmerge')

var prepareIpaForOta = require('./prepare-ipa-for-ota')
var errors = require('../errors')
var git = require('../git')
var settings = require('../settings')
var globalEvents = require('../events')
var uploadFile = require('./upload')
var apps = require('../apps')

var queue = []
var currentBuild

var setTargetScript = 'node ' + settings.iosCodeDir + '/scripts/set-current-target.js '

function getQueue() {
	return queue.slice()
}

function addToQueue(build) {
	if(!queue.find(function(b) {
		return b.app == build.app && b.revision == build.revision
	})) {
		queue.push(build)
		globalEvents.emit('build-queued', queue, build)
	}
	processQueue()
	return queue
}

function processQueue() {
	if(currentBuild) {
		return
	}
	if(queue.length == 0) {
		return
	}
	var nextBuild = queue[0]
	var tasksBeforeBuild = 1
	var tasksAfterBuild = 3
	var taskCount = tasksBeforeBuild + tasksAfterBuild
	var currentTask = 0

	function emit(event) {
		var progress = currentTask < taskCount ? currentTask / taskCount : 1
		nextBuild.progress = progress
		var build = {
			app: nextBuild.app,
			revision: nextBuild.revision,
			progress: progress,
			message: nextBuild.message,
		}
		globalEvents.emit(event, queue, build)
	}
	function bumpTask(message, taskCount) {
		if(taskCount == null) {
			currentTask++
		} else {
			currentTask = tasksBeforeBuild + taskCount
		}
		nextBuild.message = message
		emit('build-updated')
	}

	nextBuild.message = 'Checking out the right version'
	emit('build-started')

	var distServerPrefix = function(file) {
		var url = 'http://localhost/apps/'
		if(settings.distServer) {
			url = settings.distServer.url
		}

		return url + '/' + nextBuild.app + '/' + nextBuild.revision + '/' + file
	}

	var outputDir = path.join.bind(path, settings.downloadableAppsDir, nextBuild.app, nextBuild.revision)
	var codeDir = settings.iosCodeDir
	var confDir = path.join(codeDir, 'scripts')

	currentBuild = Q.all([
		prepareCodeDir()
			.then(function() {
				bumpTask('Preparing for compilation')
				return readFile(path.join(confDir, 'config.json'))
					.then(JSON.parse)
					.then(function(config) {
						return compile(config)
							.then(function(artifactDir) {
								bumpTask('Repackaging for download')
								return {
									artifactDir: artifactDir || path.join(confDir, config.deploy.output),
									config: config,
								}
							})
					})
			}),
		mkdirp(outputDir('.')),
	])
		.catch(function(err) {
			return rimraf(outputDir('.'))
				.thenReject(err)
		})
		.then(function(results) {
			var settings = results[0]
			return prepareForDownload(settings.artifactDir)
		})
		.then(function(filesToUpload) {
			bumpTask('Uploading files')
			return uploadBuildFiles(filesToUpload.files)
				.catch(function(err) {
					if(err.message == 'write EPIPE') {
						return uploadBuildFiles(filesToUpload.files)
					}
					throw err
				})
				.then(function() {
					return apps.reloadS3()
						.catch(function(e) {
							// ignore the error, it means that we don't use S3
						})
				})
				.then(removeBuildArtefacts, function(err) {
					return removeBuildArtefacts().then(function() {
						throw err
					})
				})
		})
		.finally(function() {
			queue.shift()
		})
		.then(function() {
			currentTask ++
			emit('build-finished')
		})
		.catch(function(error) {
			console.error('Build failed:', error.stack || error.message)
		})
		.finally(function() {
			currentBuild = null
			processQueue()
		})

	function prepareCodeDir() {
		return git.checkout(nextBuild.revision, codeDir)
			.then(function() {
				return exec(setTargetScript + nextBuild.app)
					.catch(function(err) {
						throw new Error('Failed when setting target: ' + err.message)
					})
			})
	}

	function compile(config) {
		var result = xcode.build(confDir, config)
		result
			.on('totalTasks', function(count) {
				taskCount += count
			})
			.on('message', function(msg) {
				bumpTask(msg.message, msg.current)
			})
			.pipe(fs.createWriteStream('/Users/benjamin/build.log'))

		// build is done
		return Q(result)
			.catch(function(e) {
				console.error('Error with build:', e, e.stack)
				throw new Error('build error: ' + e.message)
			})
	}

	function prepareForDownload(artifactDir) {
		// find originalIpa && originalItunesIcon
		return Q.all([
			readdir(artifactDir)
				.then(function(files) {
					return files.find(function(file) {
						return file.endsWith('ipa')
					})
				})
				.then(function(file) {
					if(!file) throw new Error('no ipa generated?!')
					return path.join(artifactDir, file)
				}),
			Q(path.join(codeDir, 'products', nextBuild.app))
				.then(function(dir) {
					return readdir(dir)
						.then(function(files) {
							var file = files.find(function(file) {
								return file.toLowerCase().includes('itunes')
							})
							if(!file) throw new Error('No iTunes artwork')
							return path.join(dir, file)
						})
				}),
		])
		.spread(function(originalIpa, originalItunesIcon) {
			var serverPrefix = distServerPrefix('')
			serverPrefix = serverPrefix.substring(0, serverPrefix.length - 1)
			var metadata = { branch: nextBuild.branch, revision: nextBuild.revision }
			return prepareIpaForOta(originalIpa, outputDir('.'), serverPrefix, metadata)
				.then(function(results) {
					var name = translateOutputName(originalItunesIcon)
					var outputPath = path.join(results.dir, name)
					return copy(originalItunesIcon, outputPath)
						.then(function() {
							results.files.push({
								name: name, localPath: outputPath,
							})
							return results
						})
				})
		})
	}

	function uploadBuildFiles(files) {
		if(!settings.distServer) return Q()
		return Promise.all(files.map(function(file) {
			return uploadFile({
				name: file.name,
				localPath: file.localPath,
				remotePath: distServerPrefix(file.name),
			})
		}))
	}

	function removeBuildArtefacts() {
		if(!settings.distServer) return Q()
		return rimraf(outputDir('.'))
	}
}

function translateOutputName(input) {
	input = input.toLowerCase()
	if(input.endsWith('.ipa')) {
		return 'app.ipa'
	}
	if(input.endsWith('icon120.png')) {
		return 'icon.png'
	}
	if(input.endsWith('plist')) {
		return 'info.plist'
	}
	if(input.includes('itunes')) {
		return 'itunes.png'
	}
	throw new Error('Unknown input file:', input)
}
