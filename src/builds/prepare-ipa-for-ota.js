var path = require('path')
var fs = require('fs')
var Decompress = require('decompress-zip')
var plist = require('plist')
var child_process = require('child_process')
var temp = require('temp').track()
var os = require('os.js')

module.exports = function(ipa, outputDir, distServerUrlPrefix, metadata) {
	var resultingPaths = {
		files: [],
	}
	return new Promise(function(resolve, reject) {
		temp.mkdir('ipa-contents', function(err, path) {
			err ? reject(err) : resolve(path)
		})
	})
		.then(function(artifactDir) {
			resultingPaths.dir = artifactDir
			return new Promise(function(resolve, reject) {
				var decompressor = new Decompress(ipa)
				decompressor.extract({
					path: resultingPaths.dir,
					filter: function(file) {
						var path = file.path.toLowerCase()
						return path.endsWith('.app/info.plist')
							|| path.endsWith('icon120.png')
					},
				})
				decompressor
					.on('extract', resolve)
					.on('error', reject)
			})
		})
		.then(function(extracted) {
			return Promise.all(extracted
				.map(function(file) {
					var input = path.join(resultingPaths.dir, file.stored || file.deflated)
					var output = translateOutputName(input)
					return [ input, output ]
				})
				.concat([
					[ ipa, translateOutputName(ipa) ],
				])
				.map(function(paths) {
					var outputPath = path.join(outputDir, paths[1])
					resultingPaths.files.push({
						name: paths[1],
						localPath: outputPath,
					})
					return new Promise(function(resolve, reject) {
						fs.rename(paths[0], outputPath, function(err) {
							if(err) return reject(err)
							resolve(outputPath)
						})
					})
				})
			)
		})
		.then(function(copiedFiles) {
			var plistFile = copiedFiles.find(function(file) {
				return file.endsWith('.plist')
			})
			return parsePlist(plistFile)
		})
		.then(function(parsedPlist) {
			var installPlist = {
				items: [{
					assets: [ {
						kind: 'software-package',
						url: distServerUrlPrefix + '/app.ipa',
					}, {
						kind: 'display-image',
						url: distServerUrlPrefix + '/icon.png',
					}, {
						kind: 'full-size-image',
						url: distServerUrlPrefix + '/itunes.png',
					} ],
					metadata: {
						'bundle-identifier': parsedPlist.CFBundleIdentifier,
						'bundle-version': parsedPlist.CFBundleVersion,
						kind: 'software',
						title: parsedPlist.CFBundleName,
					},
				}],
			}
			return new Promise(function(resolve, reject) {
				var writePath = path.join(outputDir, 'install.plist')
				resultingPaths.files.push({
					name: 'install.plist',
					localPath: writePath,
				})
				fs.writeFile(writePath, plist.build(installPlist), function(err) {
					err ? reject(err) : resolve()
				})
			})
				.then(function() {
					return new Promise(function(resolve, reject) {
						var md = {
							version: plist.CFBundleShortVersionString,
							buildVersion: parsedPlist.CFBundleVersion,
							name: parsedPlist.CFBundleDisplayName || parsedPlist.CFBundleName,
							branch: metadata.branch,
							revision: metadata.revision,
							date: new Date().toJSON(),
						}
						var writePath = path.join(outputDir, 'metadata.json')
						resultingPaths.files.push({
							name: 'metadata.json',
							localPath: writePath,
						})
						fs.writeFile(writePath, JSON.stringify(md), function(err) {
							err ? reject(err) : resolve()
						})
					})
				})
		})
		.then(function() {
			return resultingPaths
		})
}

function parsePlist(filePath) {
	if(os.os == 'OS X') {
		// use the bundled binary
		return new Promise(function(resolve, reject) {
			child_process.exec(path.join(__dirname, '../../bplist') + ' ' + filePath, function(error, stdout, stderr) {
				if(error) return reject(new Error(stderr.toString()))
				resolve(JSON.parse(stdout.toString()))
			})
		})
	} else {
		// fallback to plist module
		return new Promise(function(resolve, reject) {
			fs.readFile(plistFile, 'utf8', function(err, data) {
				err ? reject(err) : resolve(data)
			})
		}).then(function(content) {
			return plist.parse(content)
		})
	}
}

function translateOutputName(input) {
	input = input.toLowerCase()
	if(input.endsWith('.ipa')) {
		return 'app.ipa'
	}
	if(input.endsWith('icon120.png')) {
		return 'icon.png'
	}
	if(input.endsWith('plist')) {
		return 'info.plist'
	}
	if(input.includes('itunes')) {
		return 'itunes.png'
	}
	throw new Error('Unknown input file:', input)
}
