var merge = require('fmerge')
var dispatcher = require('../dispatcher')
var constants = require('../constants')
var baseStore = require('./base')

var capabilities = Object.keys(constants.capabilities).reduce(function(all, capability) {
	all[capability] = false
	return all
}, {})

var store = module.exports = merge(baseStore, {
	hasCapability: function(key) {
		return !!capabilities[key]
	},
})

dispatcher.register(function(payload) {
	var action = payload.action
	switch(action.type) {
		case constants.actionTypes.CAPABILITY_ADDED:
			capabilities[action.capability] = true
			break
		case constants.actionTypes.CAPABILITY_REMOVED:
			capabilities[action.capability] = false
			break
		default:
			return
	}
	store.emitChange()
})
