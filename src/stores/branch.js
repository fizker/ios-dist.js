var merge = require('fmerge')
var dispatcher = require('../dispatcher')
var constants = dispatcher.constants
var baseStore = require('./base')

var branches = []

var store = module.exports = merge(baseStore, {
	getAll: function() {
		return branches
	}
})

dispatcher.register(function(payload) {
	var action = payload.action
	switch(action.type) {
		case constants.DATA_DUMP:
			branches = action.data.branches || []
			break
		default:
			return
	}
	store.emitChange()
})
