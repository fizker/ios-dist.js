var merge = require('fmerge')
var dispatcher = require('../dispatcher')
var constants = dispatcher.constants
var baseStore = require('./base')

var apps = []

var store = module.exports = merge(baseStore, {
	getAll: function() {
		return apps
	},
	getAllWithDownloads: function() {
		return store.getAll()
			.filter(app => app.downloads && app.downloads.length > 0)
	},
	get: function(name) {
		return apps.find(app => app.name == name)
	},
})

dispatcher.register(function(payload) {
	var action = payload.action
	switch(action.type) {
		case constants.DATA_DUMP:
			apps = action.data.apps || []
			break
		default:
			return
	}
	store.emitChange()
})
