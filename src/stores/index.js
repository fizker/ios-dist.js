var fajax = require('fajax')
var Q = require('q')

var dispatcher = require('../dispatcher')

module.exports = {
	app: require('./app'),
	branch: require('./branch'),
	build: require('./build'),
	capabilities: require('./capabilities'),

	load: load,

	addChangeListener: function(callback) {
		this.app.addChangeListener(callback)
		this.branch.addChangeListener(callback)
		this.build.addChangeListener(callback)
	},
	removeChangeListener: function(callback) {
		this.app.removeChangeListener(callback)
		this.branch.removeChangeListener(callback)
		this.build.removeChangeListener(callback)
	},
}

require('./transports')

function load() {
	return Q.all([
		fajax.get('/apps'),
		fajax.get('/branches'),
		fajax.get('/builds'),
	])
	.then(xhrs => xhrs.map(xhr => xhr.body))
	.spread(function(apps, branches, builds) {
		dispatcher.handleMaintenanceAction({
			type: dispatcher.constants.DATA_DUMP,
			data: {
				apps, branches, builds
			},
		})
	})
	.done()
}
