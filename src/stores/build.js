var merge = require('fmerge')
var dispatcher = require('../dispatcher')
var constants = dispatcher.constants
var baseStore = require('./base')

var builds = []
var currentBuild = null

var store = module.exports = merge(baseStore, {
	getAll: function() {
		return builds
	},
	getCurrent: function() {
		return currentBuild
	},
})

dispatcher.register(function(payload) {
	var action = payload.action
	switch(action.type) {
		case constants.DATA_DUMP:
			builds = action.data.builds || []
			break
		case constants.BUILD_QUEUE_UPDATED:
			builds = action.builds
			break
		case constants.BUILD_STARTED:
		case constants.BUILD_PROGRESSED:
			currentBuild = action.build
			var idx = builds.findIndex(function(build) {
				return build.build == currentBuild.build && build.app == currentBuild.app
			})
			builds[idx] = currentBuild
			break
		case constants.BUILD_ENDED:
			builds = builds.filter(function(build) {
				return !(build.build == currentBuild.build && build.app == currentBuild.app)
			})
			currentBuild = null
			break
		default:
			return
	}
	store.emitChange()
})
