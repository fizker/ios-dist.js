module.exports = setup

var socketIO = require('socket.io')
var events = require('./events')

function setup(httpServer) {
	var io = socketIO.listen(httpServer)

	events
		.on('build-queued', function(queue, build) {
			io.sockets.emit('build-queue', queue)
		})
		.on('build-started', function(queue, build) {
			io.sockets.emit('build-started', build)
		})
		.on('build-updated', function(queue, build) {
			io.sockets.emit('build-updated', build)
		})
		.on('build-finished', function(queue, build) {
			io.sockets.emit('build-finished', build)
			io.sockets.emit('build-queue', queue)
		})
}
