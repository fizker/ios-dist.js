module.exports = function(store, getData) {
	return {
		_storesUpdated: function() {
			if(getData) {
				this.setState(getData())
			} else {
				this.forceUpdate()
			}
		},
		getInitialState: function() {
			return getData ? getData() : {}
		},

		componentDidMount: function() {
			store.addChangeListener(this._storesUpdated)
		},
		componentWillUnmount: function() {
			store.removeChangeListener(this._storesUpdated)
		},
	}
}
