var keyMirror = require('../src/key-mirror')

module.exports = {
	actionTypes: keyMirror([
		'DATA_DUMP',
		'BUILD_ADD', 'BUILD_QUEUE_UPDATED',
		'BUILD_STARTED', 'BUILD_ENDED', 'BUILD_PROGRESSED',
		'CAPABILITY_ADDED', 'CAPABILITY_REMOVED',
	]),
	capabilities: keyMirror([
		'SOCKET_IO',
	]),
}
