var path = require('path')
var webpack = require('webpack')

var isProd = process.env.NODE_ENV == 'production'

var plugins = isProd ? [new webpack.optimize.UglifyJsPlugin()] : null

module.exports = {
	entry: path.join(__dirname, 'client.js'),
	plugins: plugins,
	output: {
		path: path.join(__dirname, 'static'),
		filename: 'app.js',
	},
	resolve: {
		extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx'],
	},
	module: {
		loaders: [
			{ test: /\.jsx?$/, loader: 'babel' },
			{ test: /\.css$/, loader: 'style!css' },
			{ test: /\.json$/, loader: 'json' },
		],
	},
	devtool: isProd ? 'source-map' : '#inline-source-map',
}
