require('./client/shims')

import React from 'react'
import { render } from 'react-dom'
var router = require('react-router')
var Route = router.Route
var fajax = require('fajax')
var Q = require('q')

fajax.defaults({ accept: 'application/json' })
fajax.defer(Q.defer)

var Main = require('./client/main-page.component')
var App = require('./client/app.component')

var stores = require('./src/stores')

//require('./client/poll-builds')
//require('./client/socket.io.js')

stores.load()

var routes =
	<Route handler={Main}>
		<Route name="app" path="/apps/:app" handler={App}/>
	</Route>

router.run(routes, router.HistoryLocation,
	(Handler, state) => render(<Handler params={state.params} />, document.getElementById('root'))
)
